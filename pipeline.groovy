pipeline {
    /* Foi iniciado um comentário com Múltiplas linhas.
	Este texto será ignorado durante a execução do pipeline.
	
	Comentários de uma única linha são iniciados com //
	*/
	agent any
	environment {
		VERSAO_REGISTRY = null
		
		// Aspas simples servem para unificar o texto
		WORKSPACE_DIR				=	pwd()
		URL_SONARQUBE				=	'http://ci-server.domain.com.br:9000'
		PROJECT						=	'app_livro'
		DEPLOY_TEST_ENV				=	'test'
		DEPLOY_HOMOLOG_ENV			=	'homolog'
		DIR_HOMOLOG_ENV				=	'/home/jenkins/'
        DIR_HOSTFILE				=	'/home/jenkins'
		DIR_TMP				        =	'/tmp'
		MAIL_SYSADMIN				=	'ranjos@globomail.com'
		MAIL_TEAM					=	'desenv@domain.com.br'
		
		HOSTFILE = "${DIR_HOSTFILE}/${PROJECT}_${DEPLOY_TEST_ENV}.txt"
	}
	
	stages{
		stage('Checkout Projects'){
			steps{
				// Usando o echo para exibir uma mensabem
				echo 'checkout do projeto do Git...'
				
				//Usando o 'sh' para acessar o shell bash do node para executar comandos
				
				sh "mkdir -p $WORKSPACE_DIR/app/;"
				
				//Acessando o diretório de trabalho e executando comandos dentro desse diretorio
				dir("${WORKSPACE_DIR}/app/"){
					// Checkout do projeto
					git branch: 'master',
					url: 'https://github.com/jhipster/jhipster-registry'
				}
			}
		}
		stage('Code_Analysis_Build'){
			// Se houver qualquer falha em um dos stages executados paralelamente
			// o pipeline será interrompido e os stages seguints não serão executados
			failFast true
			
			//Executansdo stages paralelos para ganhar tempo
			parallel{
				stage('App1'){
					steps{
						dir("${WORKSPACE_DIR}/app/"){
							echo 'Simulando a compilaçao da aplicação 1'
						}
					}
				}
				stage('App2'){
					steps{
						dir("${WORKSPACE_DIR}/app/"){
							echo 'Simulando a compilação da aplicação 2'
						}
					}
				}
				stage('App3'){
					steps{
						dir("${WORKSPACE_DIR}/app/"){
							echo 'Simulando a compilação da aplicação 3'
						}
					}
				}
            }
        }
        stage('Build Docker Imagens'){
            steps{
                echo 'Simulando a geração de uma imagem Docker de cada app...'
            }
        }
        stage('Deploy'){
            failFast true
            parallel{
                stage('Deploy @Test'){
                    steps{
                        echo 'Deploy no ambiente de homologação...'
                    }
                }
                stage('Deploy @Homolog'){
                    steps{
                        echo 'Deploy no ambiente de homolocação...'
                    }
                }
            }
        }
        /* Se tudo ocorrer como sucesso nos stages anteriores, uma 
        mensagem será enviada ao time de desenvolvimento com cópia para
        o sysadmin
        */
        stage('Mail to Team'){
            steps{
                echo "Enviando email !!!"
            }
        }
    }
    /* Se o pipeline der certo ou não, uma mensagem sera enviada
    ao sysadmin com mais detalhes
    */
    post {
        always{
            echo "Deu erro!!"
        }
    }
}